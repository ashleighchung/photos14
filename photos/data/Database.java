package photos.data;

import photos.src.Photos.*;

import java.io.*;
import java.util.ArrayList;

public class Database{
    public static final String Dir = "photos/data";
    public static final String usersFile = "user.dat";
    static final long serialVersionUID  = 1L;

    /*
    Reads an object from memory
     */
    //Admin & User
    public static ArrayList<User> read() throws IOException, ClassNotFoundException{
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(Dir + File.separator + usersFile));

            ArrayList<User> list = (ArrayList<User>) ois.readObject();
            return list;
        }catch(Exception e){
            return new ArrayList<User>();
        }
    }
    /*
    Writes an object into memory
     */
    //Admin & User
    public static void write(ArrayList<User> obj) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream (new FileOutputStream(Dir + File.separator + usersFile));
        oos.writeObject(obj);//I should write the whole array back
    }


}

