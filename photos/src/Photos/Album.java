package photos.src.Photos;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Album Class
 * Contains fields/methods for an album (Stock or User Account)
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class Album implements Serializable {

    /**
     * Name of Album
     */
    public String albumName;

    /**
     * Which User this album belongs to
     */
    public User user;

    /**
     * All of the photos in this album
     */
    public ArrayList<Photo> listOfPhotos;

    /**
     * The earliest Date
     */
    public LocalDate minDate;

    /**
     * The latest Date
     */
    public LocalDate maxDate;

    //ADDTAG
    /**
     * addTag Field
     */
    @FXML
    TextField addTagField;

    /**
     * duplicateAnswer Field
     */
    @FXML
    TextField duplicateAnswerField;

    /**
     * sampleTagNames field
     */
    @FXML
    TextArea sampleTagNames;

    //DELETETAG
    /**
     * deleteTag Field
     */
    @FXML
    TextField deleteTagField;

    /**
     * currentTags field
     */
    @FXML
    TextArea currentTags;

    //COPYPHOTO
    /**
     * copyPhoto Field
     */
    @FXML
    TextField copyPhotoField;

    //MOVEPHOTO
    /**
     * movePhoto Field
     */
    @FXML
    TextField movePhotoField;

    //CAPTIONPHOTO
    /**
     * newCaption Field
     */
    @FXML
    TextField newCaptionField;

    /**
     * Album tilePane
     */
    @FXML
    TilePane tilePane;

    /**
     * Album scrollPane
     */
    @FXML
    ScrollPane scrollPane;

    /**
     * CSS for tilepane
     */
    String cssLayout = "-fx-border-color: red;\n";

    /**
     * What holds the img, caption in a tilepane element
     */
    static VBox oldVBox;

    /**
     * Used for selection in TilePane
     */
    class Tile{
        /**
         * index of what is selected
         */
        int index;

        /**
         * whether it is selected or not
         */
        boolean isSelected;

        /**
         * Constructor for tile
         * @param index
         */
        Tile(int index){
            this.index = index;
            this.isSelected = false;
        }

        /**
         * To deselect a tilepane element
         */
        void toggleSelection(){
            this.isSelected = !this.isSelected;
        }
    }

    /**
     * Album Constructor
     */
    public Album(){
    }

    /**
     * Album constructor
     *
     * @param albumName
     */
    public Album(String albumName){
        this.user = (User) Photos.currentUser;//not sure if we ever use this
        this.albumName = albumName;
        this.listOfPhotos = new ArrayList<Photo>();
    }

    /**
     * Album constructor with a pre-defined list of photos
     *
     * @param albumName
     * @param listOfPhotos
     */
    public Album(String albumName, ArrayList<Photo> listOfPhotos){
        this.user = (User) Photos.currentUser;//not sure if we ever use this
        this.albumName = albumName;
        this.listOfPhotos = listOfPhotos;
    }

    /**
     * @return albumName
     */
    public String getAlbumName(){
        return albumName;
    }

    /**
     * @param newAlbumName
     */
    public void setAlbumName(String newAlbumName){
        albumName = newAlbumName;
    }

    /**
     * @return User
     */
    public User getUser(){
        return user;
    }

    /**
     * @return listOfPhotos
     */
    public ArrayList<Photo> getListOfPhotos(){
        return listOfPhotos;
    }

    /**
     * Used to open up a particular album
     *
     * @param event
     */
    public void start(ActionEvent event){
        update(event);
    }

    /**
     * Sets up the tilepane when album is opened
     *
     * @param event
     */
    public void update(ActionEvent event){
        tilePane.getChildren().clear();
//        tilePane = new TilePane();
//        tilePane.setHgap(20);
//        tilePane.setVgap(20);
//        tilePane.setPadding(new Insets(20));
//        tilePane.setPrefColumns(2);
        for (int i = 0; i < Photos.listOfPhotos.size(); i++) {
            /**
             * The photo that needs to be inserted in tilepane
             */
            Photo photo = Photos.listOfPhotos.get(i);
            /**
             * Turn photo into an image
             */
            Image img = new Image("file:" + photo.getPath(), 200, 200, true, true);
            /**
             * Where to place said image
             */
            ImageView iview = new ImageView(img);
            iview.setPickOnBounds(true);
            iview.setFitWidth(225);
            iview.setFitHeight(200);
            iview.setPreserveRatio(true);
            /**
             * Photo's caption
             */
            Text txt = new Text(photo.getCaption());
            txt.setFont(new Font("Times New Roman", 13.5));
            /**
             * Space between img and caption
             */
            Region spacer = new Region();
            spacer.setMaxHeight(1);
            spacer.setMinHeight(1);
            /**
             * vbox that holds it all together
             */
            VBox box = new VBox(10, iview, spacer, txt);
            box.setUserData(new Tile(i));
            box.setVgrow(spacer, Priority.ALWAYS);
            box.setAlignment(Pos.CENTER);
            box.setOnMouseClicked((EventHandler<MouseEvent>) mouseEvent -> {
                if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                    /**
                     * Tile object
                     */
                    Tile tile = (Tile) box.getUserData();
                        tile.toggleSelection();
                        if(tile.isSelected){
                            if(oldVBox != null) {
                                oldVBox.setStyle("");
                                /**
                                 * old tile
                                 */
                                Tile oldTile = (Tile) oldVBox.getUserData();
                                oldTile.toggleSelection();
                            }
                            box.setStyle(cssLayout);//new selection
                            oldVBox = box;
                            Photos.selectedPhoto = Photos.listOfPhotos.get(tile.index);
                        }else{
                            box.setStyle("");
                            oldVBox = null;
                            Photos.selectedPhoto = null;
                        }
                }
            });
            tilePane.getChildren().add(box);
        }
        scrollPane.setContent(tilePane);
    }
    /**
     * Imports photo from Finder into album
     *
     * @param event
     */
    public void importPhoto(ActionEvent event) throws ParseException {
        if(Photos.currentUser.username.equals("stock") && Photos.currentUser.getAlbum("Stock Pictures").getListOfPhotos().size() == 10){
            PhotosController.alert("Stock Account Error", "You cannot have more than 10 pictures in Stock");
            return;
        }
        /**
         * The filechooser
         */
        FileChooser fc = new FileChooser();
        /**
         * list of files
         */
        List<File> files = fc.showOpenMultipleDialog(null);
        if(files == null){
            return;
        }
        if(Photos.currentUser.username.equals("stock") && Photos.currentAlbum.albumName.equals("Stock Pictures") && (files.size() + Photos.currentUser.getAlbum("Stock Pictures").getListOfPhotos().size()) > 10){
            PhotosController.alert("Stock Account Error", "You cannot have more than 10 pictures in Stock");
            return;
        }
        /**
         * format used to print dates
         */
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if(files != null){
            /**
             * arraylist of new photos imported
             */
            ArrayList<Photo> newPhotos = new ArrayList<Photo>();
            for(int i = 0; i < files.size(); i++){
                /**
                 * file of new photo
                 */
                File file = files.get(i);
                newPhotos.add(new Photo(sdf.parse(sdf.format(file.lastModified())), file.getPath()));
                /*
                use print(sdf.format(this.date)) to print in the desired format (defined by sdf) for UI
                 */
            }
            Photos.listOfPhotos.addAll(newPhotos);
        }
        save();
        update(event);
    }

    /**
     * Deletes photo from an album
     *
     * @param event
     */
    @FXML
    public void deletePhoto(ActionEvent event){
        if(Photos.currentUser.username.equals("stock") && Photos.currentUser.getAlbum("Stock Pictures").getListOfPhotos().size() == 5){
            PhotosController.alert("Stock Account Error", "You cannot have less than 5 pictures in Stock");
            return;
        }
        //ALERTS
        if(oldVBox == null){
            PhotosController.alert("Selection Error", "Please select a photo to delete");
            return;
        }
        //Confirmation
        if(!PhotosController.confirm("Delete Confirmation", "Are you sure you want to delete?")){
            return;
        }
        /**
         * index of what photo is deleted
         */
        int index = ((Tile) oldVBox.getUserData()).index;

        Photos.listOfPhotos.remove(index);

        oldVBox = null;
        Photos.selectedPhoto = null;

        save();
        update(event);
    }

    /**
     * Re-captions photo in an album
     *
     * @param event
     */
    @FXML
    public void captionPhoto(ActionEvent event) throws IOException {
        /**
         * new caption for photo
         */
        String newCaption = newCaptionField.getText().strip();

        //ALERTS
        if(oldVBox== null){
            PhotosController.alert("Selection Error", "Please select a photo to caption");
            return;
        }
        /**
         * index of what photo is renamed
         */
        int index = ((Tile) oldVBox.getUserData()).index;

        /**
         * find the photo in global list
         */
        Photo p = Photos.listOfPhotos.get(index);
        p.setCaption(newCaption);

        backToPhotos(event);
    }

    /**
     * Deletes photo from album and moves it to another album
     *
     * @param event
     */
    @FXML
    public void movePhoto(ActionEvent event) throws IOException {
        /**
         * album name of moved photo
         */
        String albumName = movePhotoField.getText().strip();

        //ALERTS
        if(oldVBox == null){
            PhotosController.alert("Selection Error", "Please select a photo to move");
            return;
        }
        if(albumName.length() == 0){
            PhotosController.alert("Text Error", "Please enter the album name you wish to move the photo to");
            return;
        }

        /**
         * index of what photo is moved
         */
        int index = ((Tile) oldVBox.getUserData()).index;
        /**
         * get photo from global list
         */
        Photo p = Photos.listOfPhotos.get(index);//get the photo

        for(int i = 0; i < Photos.listOfPhotos.size(); i++){
            if(Photos.listOfPhotos.get(i) == p){
                /**
                 * retrieve destination album from global list
                 */
                Album a = Photos.currentUser.getAlbum(albumName);
                if(Photos.currentUser.username.equals("stock") && albumName.equals("Stock Pictures") && a.getListOfPhotos().size() == 10){
                    PhotosController.alert("Stock Account Error", "You cannot have more than 10 pictures in Stock");
                    return;
                }
                if(a == null){
                    /**
                     * if you need to create a new album
                     */
                    ArrayList<Photo> newAlbum = new ArrayList<Photo>();//creating an array of photos
                    newAlbum.add(p);//add the photo
                    Photos.listOfAlbums.add(new Album(albumName, newAlbum));//add new album to users album
                }else{
                    a.getListOfPhotos().add(p);
                }
                Photos.listOfPhotos.remove(i);
                break;
            }
        }
          backToPhotos(event);
    }

    /**
     * Makes a copy of the photo into another album
     *
     * @param event
     */
    @FXML
    public void copyPhoto(ActionEvent event) throws IOException {
        /**
         * album name of copied photo
         */
        String albumName = copyPhotoField.getText().strip();

        //ALERTS
        if(oldVBox == null){
            PhotosController.alert("Selection Error", "Please select a photo to copy");
            return;
        }

        if(albumName.length() == 0){
            PhotosController.alert("Text Error", "Please enter the album name you wish to copy the photo to");
            return;
        }

        /**
         * index of what photo is copied
         */
        int index = ((Tile) oldVBox.getUserData()).index;
        /**
         * get photo from global list
         */
        Photo p = Photos.listOfPhotos.get(index);//get the photo
        /**
         * get the album the list is in
         */
        Album a = Photos.currentUser.getAlbum(albumName);//get the album
        if(Photos.currentUser.username.equals("stock") && albumName.equals("Stock Pictures") && a.getListOfPhotos().size() == 10){
            PhotosController.alert("Stock Account Error", "You cannot have more than 10 pictures in Stock");
            return;
        }
        if(a == null){//if album does not exist
            ArrayList<Photo> newAlbum = new ArrayList<Photo>();//creating an array of photos
            newAlbum.add(p);//add the photo
            Photos.listOfAlbums.add(new Album(albumName, newAlbum));//add new album to users album
            backToPhotos(event);
            return;
        }
        a.getListOfPhotos().add(p);
        backToPhotos(event);
    }

    /**
     * When the user presses Open to go to individual photo
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void openPhoto(ActionEvent event) throws IOException {
        if(oldVBox == null){
            PhotosController.alert("Selection Error", "Please select a song to open");
            return;
        }

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos12Gen.fxml"));
        Parent root = null;
        root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        Photo photo = loader.getController();
        photo.start(event, Photos.selectedPhoto);
    }

    /**
     * When the user presses Play to go to the slideshow
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void play(ActionEvent event) throws IOException {

        Photos.selectedAlbum = Photos.currentAlbum;

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos10Gen.fxml"));
        Parent root = null;
        root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        Slideshow slideshow = loader.getController();
        slideshow.start(event, Photos.selectedAlbum);
    }

    /**
     * Adds a tag to a photo in the album
     *
     * @param event
     */
    @FXML
    public void addTag(ActionEvent event) throws IOException {
        /**
         * tag that is added
         */
        String tag = addTagField.getText().strip();
        /**
         * whether or not this tagname allows duplicates
         */
        String b = duplicateAnswerField.getText().strip();
        //ALERTS

        /**
         * boolean for duplicates
         */
        boolean onlyOneValue = false;

        if(tag.length() == 0){
            PhotosController.alert("Text Error", "Please enter a tag");
            return;
        }
        if(!(b.equals("Yes") || b.equals("No"))){
            PhotosController.alert("Text Error", "Please enter 'Yes' or 'No'");
            return;
        }
        if(!tag.contains("=") || tag.length() < 3 || tag.indexOf("=") == 0 || tag.indexOf("=") == tag.length() - 1){
            PhotosController.alert("Text Error", "Please follow 'tag=value' format");
            return;
        }

        /**
         * selected photo in tile pane
         */
        Photo p = Photos.selectedPhoto;
        if(b.equals("No")) onlyOneValue = true;

        /**
         * New Tag obj
         */
        Tag newTag = new Tag(tag, onlyOneValue);

        for(int i = 0; i < p.listOfTags.size(); i++){
            if(p.listOfTags.get(i).tagName.equals(newTag.tagName) && p.listOfTags.get(i).tagValue.equals(newTag.tagValue)){
                PhotosController.alert("Text Error", "This tag already exists");
                return;
            }
            if(p.listOfTags.get(i).tagName.equals(newTag.tagName) && !p.listOfTags.get(i).tagValue.equals(newTag.tagValue) && p.listOfTags.get(i).onlyOneValue == true){
                PhotosController.alert("Text Error", "This tag name does not allow for duplicates");
                return;
            }
            if(p.listOfTags.get(i).tagName.equals(newTag.tagName) && !p.listOfTags.get(i).tagValue.equals(newTag.tagValue) && p.listOfTags.get(i).onlyOneValue != newTag.onlyOneValue){
                PhotosController.alert("Text Error", "A previously submitted tag with the same tag name says it allows for duplicates. Please enter 'Yes'");
                return;
            }
        }

        p.addTagToPhoto(newTag);
        backToPhotos(event);
    }

    /**
     * Deletes a tag from a photo in the album
     *
     * @param event
     */
    @FXML
    public void deleteTag(ActionEvent event) throws IOException, ParseException {
        /**
         * Tag that is deleted
         */
        String tag = deleteTagField.getText().strip();

        //ALERTS
        if(oldVBox == null){
            PhotosController.alert("Selection Error", "Please select a photo to delete a tag from");
            return;
        }

        if(tag.length() == 0){
            PhotosController.alert("Text Error", "Please enter a tag");
            return;
        }
        if(!tag.contains("=") || tag.length() < 3 || tag.indexOf("=") == 0 || tag.indexOf("=") == tag.length() - 1){
            PhotosController.alert("Text Error", "Please follow 'tag=value' format");
            return;
        }

        /**
         * Selected photo from tile pane
         */
        Photo p = Photos.selectedPhoto;
        p.deleteTagFromPhoto(tag);

        backToPhotos(event);
    }

    /**
     * Prints the album summary
     */
    public String printAlbumSummary(){
        updateDateRange();
        /**
         * Format used to print date
         */
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        /**
         * album summary
         */
        String context = "";
        context = "Name: " + albumName
                + "\nNumber of Photos: " + listOfPhotos.size() + "\nRange Of Dates: ";
        if(listOfPhotos.size() == 1){
            context = context + sdf.format(listOfPhotos.get(0).getDate()) + " to " + sdf.format(listOfPhotos.get(0).getDate());
        }
        else if(listOfPhotos.size() > 1)
            context =  context + minDate + " to " + maxDate;
        return context;
    }

    /**
     * Used to update the min and max date in an album
     */
    public void updateDateRange() {
        if(listOfPhotos.size() != 0){
            /**
             * create a list of all dates in an album
             */
            List<LocalDate> dates = new ArrayList<>();

            for(int i = 0; i < listOfPhotos.size(); i++){
                /**
                 * Date obj of date
                 */
                Date pDate = listOfPhotos.get(i).getDate();
                /**
                 * LocalDate obj of date for easier comparing
                 */
                LocalDate pLDate = LocalDate.of(pDate.getYear()+1900, pDate.getMonth()+1, pDate.getDay()+12);
                dates.add(pLDate);
            }

            maxDate = dates.stream().max(Comparator.comparing(LocalDate::toEpochDay)).get();
            minDate = dates.stream().min(Comparator.comparing(LocalDate::toEpochDay)).get();
        }
        else{
            maxDate = null;
            minDate = null;
        }
    }

    /**
     * To save progress and new changes
     */
    public void save(){
        for(int i = 0; i < Photos.listOfAlbums.size(); i++){
            if(Photos.listOfAlbums.get(i).albumName.equals(Photos.albumName)){
                Photos.listOfAlbums.get(i).listOfPhotos = Photos.listOfPhotos;
            }
        }
    }

    /**
     * Brings user back to previous page
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void backToAlbums(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos5Gen.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        //Save the new photos
        save();
        User user = loader.getController();
        user.start(event);
    }

    /**
     * Brings user to delete tag popup
     * @param event
     * @throws IOException
     */
    @FXML
    public void toDeleteTag(ActionEvent event) throws IOException {
        if(oldVBox == null){
            PhotosController.alert("Selection Error", "Please select a photo to delete a tag from");
            return;
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/PhotosDeleteTag.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        Album album = loader.getController();
        album.startDeleteTag(event);
    }

    /**
     * Used to populate the "Current Tags" field in delete popup
     *
     * @param event
     */
    public void startDeleteTag(ActionEvent event){
        ArrayList<String> currentTagNames = new ArrayList<>();
        for(int t = 0; t < Photos.selectedPhoto.listOfTags.size(); t++){
            currentTagNames.add(Photos.selectedPhoto.listOfTags.get(t).fullTag);
        }

        String tagNames = "";

        if(currentTagNames != null){
            for(int i = 0; i < currentTagNames.size(); i++){
                if(!tagNames.contains(currentTagNames.get(i)))
                    tagNames = tagNames + currentTagNames.get(i) + "\n";
            }
        }
        currentTags.setText(tagNames);
    }

    /**
     * Brings user to add tag popup
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void toAddTag(ActionEvent event) throws IOException {
        if(oldVBox == null){
            PhotosController.alert("Selection Error", "Please select a photo to add a tag to");
            return;
        }

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/PhotosAddTag.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        Album album = loader.getController();
        album.startAddTag(event);
    }

    /**
     * Used to populate the "Sample Tags" field in add tag popup
     *
     * @param event
     */
    public void startAddTag(ActionEvent event){
        /**
         * Arraylist of all user's tag names currently used
         */
        ArrayList<String> currentTagNames = new ArrayList<>();
        for(int i = 0; i < Photos.listOfAlbums.size(); i++){
            for(int j = 0; j < Photos.listOfAlbums.get(i).listOfPhotos.size(); j++){
                for(int t = 0; t < Photos.listOfAlbums.get(i).listOfPhotos.get(j).listOfTags.size(); t++){
                    currentTagNames.add(Photos.listOfAlbums.get(i).listOfPhotos.get(j).listOfTags.get(t).tagName);
                }
            }
        }

        /**
         * Sample tag names
         */
        String tagNames = Photos.sampleTagNames[0] + "\n" + Photos.sampleTagNames[1] + "\n" + Photos.sampleTagNames[2] + "\n---------";
        if(currentTagNames != null){
            for(int i = 0; i < currentTagNames.size(); i++){
                if(!tagNames.contains(currentTagNames.get(i)))
                    tagNames = tagNames + "\n" + currentTagNames.get(i);
            }
        }
        sampleTagNames.setText(tagNames);
    }

    /**
     * Brings user to copy photo popup
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void toCopyPhoto(ActionEvent event) throws IOException {
        if(oldVBox== null){
            PhotosController.alert("Selection Error", "Please select a photo to copy");
            return;
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/PhotosCopyPhoto.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Brings user to new caption popup
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void toNewCaption(ActionEvent event) throws IOException {
        if(oldVBox == null){
            PhotosController.alert("Selection Error", "Please select a photo to add a caption to");
            return;
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/PhotosNewCaption.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Brings user to move photo popup
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void toMovePhoto(ActionEvent event) throws IOException {
        if(oldVBox == null){
            PhotosController.alert("Selection Error", "Please select a photo to move");
            return;
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/PhotosMovePhoto.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Brings user out of popups
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void backToPhotos(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos9Gen.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        save();
        Album album = loader.getController();
        album.start(event);
    }
}

