package photos.src.Photos;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import photos.data.Database;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Photos Class
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class Photos extends Application {

    //admin
    /**
     * Global currentUser
     */
    public static User currentUser;
    /**
     * Global list of users
     */
    public static ArrayList<User> listOfUsers;
    /**
     * Global userName
     */
    public static String userName;

    //User
    /**
     * Global list of albums
     */
    public static ArrayList<Album> listOfAlbums;
    /**
     * Global album name
     */
    public static String albumName;
    /**
     * Global sample tag names
     */
    public static String[] sampleTagNames = {"location", "dayOfWeek", "food"};

    //Album
    /**
     * Global list of Photos
     */
    public static ArrayList<Photo> listOfPhotos;

    //Photo
    /**
     * Global selected photo
     */
    public static Photo selectedPhoto;

    //Slideshow
    /**
     * Global currentAlbum
     */
    public static Album currentAlbum;
    /**
     * Global selected album
     */
    public static Album selectedAlbum;

    /**
     * Used to start the app
     *
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        try {
            /**
             * Retrieves list of users from database
             */
            Photos.listOfUsers = Database.read();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        if(Photos.listOfUsers.size() == 0) {
            Photos.listOfUsers.add(new User("stock"));
            /**
             * Creates stock user
             */
            User stockUser = Photos.listOfUsers.get(0);
            stockUser.getListOfAlbums().add(new Album("Stock Pictures"));
            /**
             * Format used to print dates
             */
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            /**
             * Path to stock photos
             */
            final String PATH = "./photos/data/StockPictures";
            /**
             * File directory
             */
            File dir = new File(PATH);
            /**
             * list of files
             */
            File[] files = dir.listFiles();
            /**
             * Stock album
             */
            Album stockAlbum = stockUser.getListOfAlbums().get(0);
            for (File f : files) {
                try {
                    stockAlbum.getListOfPhotos().add(new Photo(sdf.parse(sdf.format(f.lastModified())), f.getPath()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        Parent root = FXMLLoader.load(getClass().getResource("/photos/src/view/Photos2Login.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Photos14 - Ashleigh Chung and Jason Cheng");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
        stage.setOnCloseRequest(e -> {
            e.consume();
            try {
                Database.write(Photos.listOfUsers);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            stage.close();
        });
    }

    /**
     * Main method
     *
     * @param args
     */
    public static void main(String [] args){
        launch(args);
    }
}
