package photos.src.Photos;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.Serializable;
import java.util.Optional;

/**
 * PhotosController Class
 *
 * @author Jason Cheng
 */
public class PhotosController implements Serializable {

    /**
     * Login text from field
     */
    @FXML
    private TextField loginText;

    /**
     * When User clicks on Login buton
     *
     * @param event
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @FXML
    public void prelogin(ActionEvent event) throws IOException, ClassNotFoundException {
        /**
         * username from text field
         */
        String username = loginText.getText().strip();
        //admin
        if (username.equals("admin")) {
            //Handle Changing scene from Login to Admin
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/photos/src/view/Photos3Admin.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();

            //Display the listview
            Admin admin = loader.getController();
            admin.start();

        } else {//has to be a user
            if(User.login(event, username)){
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/photos/src/view/Photos5Gen.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
                stage.setScene(scene);
                stage.show();

                //Dislay the photos
                User user = loader.getController();
                user.start(event);
            }else{
                alert("Login Error", "Username does not exist.");
            }
        }
    }

    /**
     * Creating alerts
     *
     * @param title
     * @param message
     */
    public static void alert(String title, String message) {
        /**
         * the alert obj
         */
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initModality(Modality.APPLICATION_MODAL);

        alert.setTitle(title);
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Confirm boxes for logouts and other things
     *
     * @param title
     * @param message
     * @return
     */
    public static boolean confirm(String title, String message) {
        /**
         * Alert object
         */
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setTitle(title);
        alert.setContentText(message);

        /**
         * Result alert
         */
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

}
