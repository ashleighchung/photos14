package photos.src.Photos;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * Slideshow Class
 *
 * @author Ashleigh Chung
 * @author Jason Cheng
 */
public class Slideshow {

    /**
     * Album in slideshow
     */
    Album album;

    /**
     * Which photo is being displayed
     */
    int index;

    /**
     * Whether image is displaed
     */
    @FXML
    ImageView slideshowImage;

    /**
     * Slideshow constructor
     *
     * @param album
     */
    public Slideshow(Album album){
        this.album = album;
    }

    /**
     * Slideshow constructor
     */
    public Slideshow(){
        //No arg constructor for FXML
    }

    /**
     * Used to start slideshow
     *
     * @param event
     * @param currentAlbum
     */
    public void start(ActionEvent event, Album currentAlbum){
        album = currentAlbum;
        index = 0;
        slideshowImage.setImage(new Image("file:" + currentAlbum.listOfPhotos.get(0).getPath().toString(), 400, 400, true, true));
    }

    /**
     * When user clicks '-->'
     *
     * @param event
     */
    @FXML
    public void next(ActionEvent event){
        index++;
        if(index >= album.listOfPhotos.size())
            index = 0;

        slideshowImage.setImage(new Image("file:" + album.listOfPhotos.get(index).getPath().toString(), 400, 400, true, true));
    }

    /**
     * When user clicks '<--'
     *
     * @param event
     */
    @FXML
    public void previous(ActionEvent event){
        index--;
        if(index <= -1)
            index = album.listOfPhotos.size() - 1;

        slideshowImage.setImage(new Image("file:" + album.listOfPhotos.get(index).getPath().toString(), 400, 400, true, true));
    }

    /**
     * Brings user back to list of photos
     *
     * @param event
     * @throws IOException
     */
    @FXML
    public void backToPhotos(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/photos/src/view/Photos9Gen.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();

        Album album = loader.getController();
        album.start(event);
    }

}

